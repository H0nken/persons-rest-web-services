package com.example.personsrest.model;

import lombok.Value;

@Value
public class UpdatePerson {
    String name;
    String city;
    int age;
}
