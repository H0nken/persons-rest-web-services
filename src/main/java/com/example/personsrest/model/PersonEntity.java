package com.example.personsrest.model;

import com.example.personsrest.domain.Person;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class PersonEntity implements Person {
    private String id;
    private String name;
    private String city;
    private int age;
    private List<String> groups;

    public PersonEntity(String name, String city, int age, List<String> groups) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.city = city;
        this.age = age;
        this.groups = groups;
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void setActive(boolean active) {

    }

    @Override
    public List<String> getGroups() {
        return this.groups == null ? List.of() : this.groups;
    }

    @Override
    public void addGroup(String groupId) {
        this.groups.add(groupId);
    }

    @Override
    public void removeGroup(String groupId) {
        this.getGroups().removeIf(group -> group.equals(groupId));
    }
}
