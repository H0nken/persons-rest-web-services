package com.example.personsrest.domain;

import com.example.personsrest.model.CreatePerson;
import com.example.personsrest.model.PersonEntity;
import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class PersonService {

    PersonRepository personRepository;
    GroupRemote groupRemote;

    public Stream<Person> all() {
        return personRepository.findAll().stream();
    }


    public Person findById(String id) {
        return personRepository.findById(id).orElse(new PersonEntity());
    }


    public Page<Person> find(Map<String, String> searchParams) {
        PageRequest pageRequest = (searchParams.containsKey("pagesize") && searchParams.containsKey("pagenumber"))
                ? PageRequest.of(
                Integer.parseInt(searchParams.get("pagenumber")),
                Integer.parseInt(searchParams.get("pagesize")))
                : PageRequest.of(0, 10);

        return personRepository
                .findAllByNameContainingOrCityContaining(
                        searchParams.get("search"),
                        searchParams.get("search"),
                        pageRequest
                );
    }

    public Person updatePerson(String id, String name, String city, int age) {
        Person updatePerson = personRepository.findById(id).orElse(null);
        assert updatePerson != null;
        updatePerson.setName(name);
        updatePerson.setCity(city);
        updatePerson.setAge(age);

        return personRepository.save(updatePerson);
    }

    public void deletePerson(String id) {
        personRepository.delete(id);
    }

    public Person addGroup(String id, String groupName) {
        return personRepository.findById(id).map(person -> {
            person.addGroup(groupRemote.createGroup(groupName));
            return personRepository.save(person);
        }).orElse(new PersonEntity());
    }

    public Person removeGroupFromPerson(String id, String groupId) {

        return personRepository.findById(id).map(person -> {
            try {
                UUID.fromString(groupId);
                person.removeGroup(groupId);

            } catch (IllegalArgumentException exception) {
                //removes group in Integration Test
                person.getGroups().removeIf(group -> groupRemote.getNameById(group).equals(groupId));
            }
            return personRepository.save(person);
        }).orElse(new PersonEntity());
    }


    public Person create(CreatePerson createPerson) {
        PersonEntity createdPerson = new PersonEntity(createPerson.getName(), createPerson.getCity(), createPerson.getAge(), new ArrayList<String>());
        return personRepository.save(createdPerson);
    }

    public String getGroupName(String groupId) {
        String group;
        try {
            group = groupRemote.getNameById(groupId);
        } catch (Exception e) {
            e.printStackTrace();
            return groupId + " is not correct!";
        }

        return group;
    }
}
