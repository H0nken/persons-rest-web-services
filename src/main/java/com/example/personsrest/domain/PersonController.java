package com.example.personsrest.domain;

import com.example.personsrest.model.CreatePerson;
import com.example.personsrest.model.PersonDTO;
import com.example.personsrest.model.UpdatePerson;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/persons")
@AllArgsConstructor
public class PersonController {
    PersonService personService;

    @GetMapping()
    public List<PersonDTO> all(@RequestParam(required = false) Map<String, String> search) {
        return !search.isEmpty()
                ? personService.find(search).stream().map(this::toDTO).collect(Collectors.toList())
                : personService.all().map(this::toDTO).collect(Collectors.toList());
    }

    @PostMapping
    public PersonDTO create(@RequestBody CreatePerson createPerson) {
        return toDTO(personService.create(createPerson));
    }


    @GetMapping("/{id}")
    public PersonDTO findById(@PathVariable("id") String id) {
        return toDTO(personService.findById(id));
    }

    @PutMapping("/{id}")
    public PersonDTO update(@PathVariable("id") String id, @RequestBody UpdatePerson updatePerson) {

            return (toDTO(
                    personService.updatePerson(
                            id,
                            updatePerson.getName(),
                            updatePerson.getCity(),
                            updatePerson.getAge())));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
            personService.deletePerson(id);
            return ResponseEntity.ok().build();
    }


    @PutMapping("/{id}/addGroup/{groupName}")
    public PersonDTO addGroup(@PathVariable("id") String id, @PathVariable("groupName") String groupName) {
        return toDTO(personService.addGroup(id, groupName));
    }

    @DeleteMapping("/{id}/removeGroup/{groupId}")
    public PersonDTO removeGroupFromPerson(@PathVariable("id") String id, @PathVariable("groupId") String groupId) {

        return (toDTO(personService.removeGroupFromPerson(id, groupId)));
    }


    private PersonDTO toDTO(Person person) {
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getCity(),
                person.getAge(),
                person.getGroups().stream()
                        .map(groupId -> personService.getGroupName(groupId))
                        .collect(Collectors.toList()));
    }


}
