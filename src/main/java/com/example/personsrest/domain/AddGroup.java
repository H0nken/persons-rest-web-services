package com.example.personsrest.domain;

import lombok.Value;

@Value
public class AddGroup {
    String id;
    String groupName;
}
